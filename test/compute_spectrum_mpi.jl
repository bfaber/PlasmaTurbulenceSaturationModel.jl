using LinearAlgebra, MPI, StellaratorOptimization, VMEC, GeneTools, PlasmaTurbulenceSaturationModel, JLD2
#ENV["JULIA_DEBUG"] = PlasmaTurbulenceSaturationModel

BLAS.set_num_threads(1) # Change this to whatever works for you architecture
MPI.Init()
VMEC.load_vmec_lib("libvmec_mkl.so")
rank = MPI.Comm_rank(MPI.COMM_WORLD)
nml = VMEC.read_vmec_namelist("/global/homes/b/bfaber/.julia/dev/StellaratorOptimization/test/input.aten")
vmec = run_vmec(MPI.COMM_WORLD, nml)
#vmec = load_object("qa_vmec.jld2")
surface = VmecSurface(0.5, vmec)
model = PTSM.setup_linear_model(surface, nkx = 41, nky = 20, soln_interval = 24π, n_points = 1024, ε = 400.0, n_ev = 5)
PTSM.compute_linear_spectrum!(model, MPI.COMM_WORLD)

if rank == 0
    PTSM.compute_couplings!(model) 
    PTSM.fill_overlap!(model)
    save_object("/pscratch/sd/b/bfaber/test_model_aten_medium_n_ev_5.jld2", model)
end
