using VMEC
using GeneTools

@testset "VMEC PTSM test" begin
    #use the ku4 eq since that's also used in other julia testing suites
    vmecname = joinpath(@__DIR__, "wout_ku4.nc")
    vmec = VMEC.readVmecWout(vmecname);
    vmecsurf = VmecSurface(0.6, vmec);
    rtol = 1.0E-8
    nky = 5
    yl = nky
    nkx = 6
    xl = 2*nkx-1
    n_points = 1024
    Δkx = 0.2
    Δky = 0.2
    n_ev = 2
    evl = n_ev^3

    msize = (n_ev ^ 3, xl, yl, nkx, nky)
    nsize = (n_ev, xl, yl)
    psize = (xl, yl, nkx, nky)
    coeff_size = (3, evl, xl, yl, nkx, nky)
    eigval_size = (3, n_ev, xl, yl)
    eigvec_size = (n_points, n_ev, xl, yl)
     

    model = PTSM.setup_linear_model(vmecsurf, Δkx = Δkx,
            Δky = Δky, soln_interval = 12π,
            n_ev = n_ev, n_points = n_points,
            solver = PTSM.ArnoldiMethod(tolerance = 1e-6));

    # test that nothing has changed in the gross parameters of model geometry
    @testset "test model geometry" begin
        l = 3362
        @test length(model.geometry.B) == l
        @test length(model.geometry.Bx∇B.∇x) == l
        @test length(model.geometry.Bx∇B.∇y) == l
        @test length(model.geometry.g.xx) == l
        @test length(model.geometry.g.xy) == l
        @test length(model.geometry.g.yy) == l
        @test length(model.geometry.jac) == l
        @test length(model.geometry.∂B∂z) == l
        #can add spot checks for actual numbers if desired here


        @test isapprox(model.geometry.parameters.ι, 1.1480088469033318, rtol=rtol)
        @test isapprox(model.geometry.parameters.s_hat, 0.24807678555834414, rtol=rtol)
        @test model.geometry.parameters.nfp == 4.0
    end

    #test that nothing has changed in grid
    @testset "test model grid" begin
        @test size(model.grid.k_perp) == (xl, yl)
        @test length(model.grid.kx_map.forward) == xl
        @test length(model.grid.kx_map.reverse) == xl
        @test model.grid.power_spectrum.kx_limit == Δkx
        @test model.grid.power_spectrum.kx_exp == 2.0
        @test model.grid.power_spectrum.ky_limit == 0.6 #why is this 0.6 and not 0.2
        @test isapprox(model.grid.power_spectrum.ky_exp, 1.3333333333333333, rtol=rtol)
        @test model.grid.Δk.kx == 0.2
        @test model.grid.Δk.ky == 0.2
    end

    #test size of model overlap (should all be zero right now)
    @testset "test model overlap before computation" begin
        @test size(model.three_wave_overlap) == msize
        @test size(model.coupling_index) == psize
        @test size(model.overlap_ranges) == psize
        @test size(model.amplitudes) == psize
    end

    @testset "plasma components of model" begin
        @test model.plasma.β == 1.0
        @test model.plasma.τ == 1.0
        @test model.plasma.∇T == 3.0
        @test model.plasma.∇n == 0.0
    end

    @testset "size of spectrum and τ, CC" begin
        @test size(model.ω) == eigval_size 
        @test size(model.β) == eigvec_size
        @test size(model.β²) == eigvec_size
        @test size(model.k_perp²) == nsize
        @test size(model.k_par²) == nsize
        @test size(model.τ) == coeff_size
        @test size(model.CC) == coeff_size
    end
end
        
