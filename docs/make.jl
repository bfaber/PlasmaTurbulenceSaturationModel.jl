using PlasmaTurbulenceSaturationModel
using Documenter

makedocs(;
    modules=[PlasmaTurbulenceSaturationModel],
    authors="Benjamin Faber <bfaber@wisc.edu> and contributors",
    repo="https://gitlab.com/wistell/PlasmaTurbulenceSaturationModel.jl/blob/{commit}{path}#L{line}",
    sitename="PlasmaTurbulenceSaturationModel.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://wistell.gitlab.io/PlasmaTurbulenceSaturationModel.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "Models" => [
                     "Three-Field ITG" => ["Overview" => "models/three_field_itg.md",
                                           "Normalizations" => "models/three_field_normalizations.md",
                                           "Continuity Equation" => "models/three_field_continuity.md",
                                           "Momentum Equation" => "models/three_field_momentum.md",
                                           "Energy Equation" => "models/three_field_energy.md",
                                           "Fourier Representation" => "models/three_field_fourier.md",],
                    ],
        "Library" => [
                      "Private" => "library/internals.md",
                     ],
    ],
)
