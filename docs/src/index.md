```@meta
CurrentModule = PlasmaTurbulenceSaturationModel
```

# PlasmaTurbulenceSaturationModel

```@index
Pages = ["overview.md"]
```
To use the `PlasmaTurbulenceSaturationModel` (`PTSM`), use the package manager to download and install:
```
julia> using Pkg;
julia> Pkg.add(url="https://gitlab.com/bfaber/PlasmaTurbulenceSaturationModel.jl.git");
julia> using PlasmaTurbulenceSaturationModel
```

## Flux-tube geometry
`PTSM` uses a _flux-tube_ approximation for the magnetic geometry.
The flux-tube approximation simulates a local volume around a central magnetic fieldline and assumes a Taylor expansion in the direction binormal to a magnetic field line:
```math
\mathbf{B}\left(x, y, z\right) = \nabla x(z) \times \nabla y(z) \approx \mathbf{B}_0\left(z\right) + \frac{\partial \mathbf{B}}{\partial x}\Delta x +  \frac{\partial \mathbf{B}}{\partial y}\Delta y + \ldots
```
[Normalizations](@ref) are consistent with the gyrokinetic code [GENE](https://genecode.org) and the geometry package GIST, where the length scale is normalized to average minor radius $`a`$ and the magnetic field magnitude is normalized to value at the last-closed flux surface $`B_a`$.
With these conventions, ``x`` is a radial-like flux coordinate and ``y`` is an angle-like coordinate.
The field line following coordinate ``z`` can be chosen to follow the toroidal or poloidal angle (and implicitly defines ``y`` in a straight field line coordinate system).
For the GENE-like coordinate system, the angle ``z = \theta`` is the poloidal angle and ``y = q \theta - \zeta`` where ``q`` is the safety factor and ``zeta`` is a toroidal-like angle.

Flux tube geometries need to be extracted from equilibrium (re)construction codes like VMEC.
To make use of VMEC equilibria, two optional packages need to be loaded with the `PlasmaTurbulenceSaturationModel`:
  - [`VMEC`](https://gitlab.com/wistell/VMEC.jl)
  - [`GeneTools`](https://gitlab.com/wistell/GeneTools.jl)

## Setting up a PTSM model
A fluid model is represented by the [FluidModel](@ref) type, a composite type that holds both the parameters for performing saturation calculations and the spectrum of the subsequent linear mode calculations.
A fluid model is intialized from an equilibrium geometry and defined by keyword parameters:
```julia
using PlasmaTurbulenceSaturationModel, VMEC, GeneTools

vmec = read_vmec_wout("wout_file.nc")

model = PTSM.setup_linear_model(vmec; # The VMEC flux surface to perform the saturation calculation
                                n_fields=3, # Number of model fields
                                n_ev=1, # Number of eigenvalues 
                                prec=Float64, # Precision for the calculations
                                nkx=21, # Number of positive and negative kx points including kx = 0
                                nky=10, # Number of ky points
                                Δkx = 1/div(nkx, 2), # Spacing between kx points
                                Δky = 1/nky, # Spacing between ky points
                                kxSpecLimit = 0.2, # Energy spectrum transition point (kx)
                                kySpecLimit = 0.6, # Energy spectrum transition point (ky)
                                kxSpecPower = 2.0, # Energy spectrum decay power (kx)
                                kySpecPower = 4/3, # Energy spectrum decay power (ky)
                                ∇T = 3.0, # Normalized temperature gradient (-a/T dT/dx)
                                ∇n = 0.0, # Normalized density gradient (-a/n dn/dx)
                                τ = 1.0, # Ratio of electron-to-ion temperature
                                β = 1.0, # Plasma beta
                                solver=ArnoldiMethod(), # Method used solving linear eigenproblem
                                soln_interval=8π, # Length along field line over which to compute linear eigenproblem (radians)
                                ε=400.0, # Hyperdiffusion coefficient for numerical stability
                                errorOrder=4, # Order of the finite difference operators
                                hyperdiffusionOrder=2, # Order for the hyperdiffusion operator
                                n_points = 1024, # Number of points over which the linear eigenvalue solution domain is discretized
                               )
```

The resulting `FluidModel` has eight fields:
- `plasma`: Holds the plasma parameters necessary for the model
- `grid`: Defines the spectral grid where eigenvalue solutions will be calculated
- `problem`: Defines the parameters controling the linear eigenvalue problem
- `geometry`: Holds the magnetic field line geometry
- `spectrum`: Holds the eigenvalues and eigenmodes
- `overlap_range`: Holds the range 
- `overlap`: Holds the overlap of pairs of eigenmodes
- `τC`: Holds the three wave interaction times and coupling coefficients

### Key parameters for setting up a model
The default values given by the keyword parameters in `setup_linear_model` are sufficient for quick calculations of a spectrum for a given `VmecSurface` object.
The default values are not going to yield converged results in general, however, as the convergence of the eigenvalue problem will be highly dependent on magnetic geometry.
The important parameters controlling the accuracy of the eigenvalue problem are the `soln_interval` and `n_points` parameters.
The linear eigenvalue problem is accomplished by selecting a domain of size `soln_interval` centered on the ballooning angle for a particular `(kx, ky)` mode and discretizing with `n_points` points.
Currently, the same `soln_interval` is used for every mode, thus the `soln_interval` must be large enough to resolve the largest eigenmode in the system.
For low-magnetic-shear stellarators, the eigenmodes at low `ky` can be very extended along field lines with extents exceeding 32π radians.
If the `soln_interval` is increased to accomodate these modes, the number of points used in the discretization must also increase accordingly.
The number of points required for converged solutions is configuration dependent, but a effective rule-of-thumb is to use 1024 points for every 8π radians in the `soln_interval`.

### Wavenumber grid (`SpectralGrid`)
Properties of the wavenumber space where the eigenmodes and coupling calculations will be calculated is defined by the [SpectralGrid](@ref) type.
The size of the grid of wavenumbers over which `PTSM` will compute the eigenmode solution is defined by both the number of desired modes `nkx(y)` and/or the spacing between modes `Δkx(y)`.
The wavenumbers are normalized to the ion sound radius $`\rho_s`$; as such, the fluid model is valid _only_ for $`\mathbf{k}_\perp \rho_s`$, to enfore this constraint, the fluid model will only keep `kx <= 1` and `ky <= 1`.
When specifying the spectral grid, the `Δkx(y)` parameter will take precedence over the `nkx(y)` parameter.
For example, if `nkx = 11` but the desired spacing between `kx` modes is `Δkx = 0.2`, the number of `kx` modes retained in the fluid model will be `nkx = 6` (`kx = 0.0, 0.2, 0.4, 0.6, 0.8, 1.0`).

For the coupling calculation, the nonlinear amplitudes of the modes involved is estimated by weighting the amplitude by a model power spectrum.
This spectrum provides four free parameters to the model `kxSpecLimit`, `kySpecLimit`, `kySpecPower`, `kxSpecPower` and is defined as:
```math
    \beta\left(k_x, k_y) \propto 1 \text{for } k_x \leq \text{kxSpecLimit}; \frac{k_x}{\text{kxSpecLimit}}^{\text{kxSpecPower}} \text{ for } k_x > \text{kxSpecLimit}\\
    \beta\left(k_x, k_y) \propto 1 \text{for } k_y \leq \text{kySpecLimit}; \frac{k_y}{\text{kySpecLimit}}^{\text{kySpecPower}} \text{ for } k_y > \text{kySpecLimit}
```
### 
## Computing the linear spectrum and mode couplings
Once a `FluidModel` is defined, the commands for computing the spectrum and evaluating the three-wave correlation times and coupling coefficients are straight forward.
The `compute_linear_spectrum!` function will compute the eigenvalues and eigenmodes at each of the points defined in the `SpectralGrid` and populate the results into the `spectrum` field of the `FluidModel`.
With the spectrum computed, the `compute_couplings!` function will compute the three-wave interaction times and the complex coupling coefficients for each possible admissible interaction between eigenmodes.
The resulting coupling values are recorded in the `τC` field of the `FluidModel`.
This is illustrated below:
```julia-repl
julia> using PlasmaTurbulenceSaturationModel, VMEC, GeneTools

julia> vmec = read_vmec_wout("wout_file.nc");

julia> vmec_surface = VmecSurface(0.5, vmec);

julia> model = PTSM.setup_linear_model(vmec_surface; kwargs...);

julia> PTSM.compute_linear_spectrum!(model)

julia> PTSM.compute_couplings!(model)
```