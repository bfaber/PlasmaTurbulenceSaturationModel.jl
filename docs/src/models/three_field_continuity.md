# Derivation of the ion continuity equation

The first order ion continuity equation reads
```math
\frac{\partial n_1}{\partial t} + n_0\nabla\cdot\mathbf{v}_E + \nabla\cdot\left(n_i \mathbf{v}_{i\ast}\right) + \nabla\cdot\left(n_i \mathbf{v}_{pi} + n_i
\mathbf{v}_{\pi i}\right) + \hat{\mathbf{b}} \cdot\nabla\left(n_0 v_\parallel\right) = 0.
```
The ``\mathbf{E}\times\mathbf{B}`` velocity ``\mathbf{v}_E`` is given by
```math
\mathbf{v}_E = \frac{1}{B}\hat{\mathbf{b}}  \times \nabla\phi_1 = \frac{T_{e0}}{eB_a\hat{\mathbf{B}} }\frac{\rho_\mathrm{s}}{a}\hat{\mathbf{b}} \times\frac{\hat{\nabla}}{\rho_\mathrm{s}}\hat{\phi}_1
= \frac{c_s^2}{\Omega_i}\frac{\rho_\mathrm{s}}{a\hat{\mathbf{B}} \rho_\mathrm{s}}\hat{\mathbf{b}} \times\hat{\nabla}\hat{\phi}_1 =
c_s\frac{\rho_\mathrm{s}}{a}\frac{1}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times\hat{\nabla}\hat{\phi}_1.
```
The divergence of ``\mathbf{v}_E`` is given at lowest order in ``\rho_\mathrm{s}/a`` by
```math
\nabla\cdot\mathbf{v}_E \approx -c_s\frac{\rho_\mathrm{s}}{a}\frac{1}{\hat{\mathbf{B}}^2}\nabla\hat{\mathbf{B}} \cdot \hat{\mathbf{b}} \times\hat{\nabla}\hat{\phi}_1 =
\frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\frac{1}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times \frac{\hat{\nabla}\hat{\mathbf{B}} }{\hat{\mathbf{B}} }\cdot\hat{\nabla}\hat{\phi}_1 =
\frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\frac{2}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times\hat{\kappa}\cdot\hat{\nabla}\hat{\phi}_1
```
where ``\hat{\nabla}_\perp\hat{\mathbf{B}} /(a\hat{\mathbf{B}} ) \approx 2\mathbf{\hat{\kappa}}/a``.  Similarly, the diamagnetic drift ``\mathbf{v}_{i\ast}`` is defined as
```math
\begin{aligned}
\mathbf{v}_{i\ast} &= \frac{1}{n_i e B}\hat{\mathbf{b}}  \times \nabla n_i T_i = \frac{n_{e0}T_{e0}}{n_{e0} \hat{n}_0 e B_a \hat{\mathbf{B}} }\hat{\mathbf{b}} \times\nabla\left(\hat{n}_0 \hat{T}_0 + \frac{\rho_\mathrm{s}}{a}\hat{n}_0\hat{T}_1 + \frac{\rho_\mathrm{s}}{a}\hat{n}_1
\hat{T}_0\right) \\
&= \frac{c_s^2}{\Omega_i\hat{\mathbf{B}} }\hat{\mathbf{b}} \times\left(\frac{\hat{\nabla}}{a}\hat{T}_0 + \frac{\rho_\mathrm{s}}{a}\frac{\hat{\nabla}}{\rho_\mathrm{s}}\hat{T}_1  + \frac{\rho_\mathrm{s}}{a}\frac{\hat{\nabla}}{\rho_\mathrm{s}}\frac{\hat{n}_1}{\hat{n}_0}\hat{T}_0 + \frac{\rho_\mathrm{s}}{a}\frac{\hat{n}_1}{\hat{n}_0}\frac{\hat{\nabla}}{a}\hat{T}_0\right) \\
&= c_s\frac{\rho_\mathrm{s}}{a}\frac{1}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times\left(\hat{\nabla}\hat{T}_0 + \hat{\nabla}\hat{T}_1 + \frac{\hat{T}_0}{\hat{n_0}}\hat{\nabla}\hat{n}_1 +
\frac{\rho_\mathrm{s}}{a}\frac{\hat{n}_1}{\hat{n}_0}\hat{\nabla}\hat{T}_0\right)
\end{aligned}
```
Taking the divergence of ``\nabla\cdot(\left(n_i \mathbf{v}_{i\ast}\right)`` introduces a ``\hat{\nabla}\hat{n}_1`` term that then cancels with the
third term in the previous equation.  The divergence of ``\mathbf{v}_{i\ast}`` introduces a higher order correction due to because of the
``\frac{1}{a}\hat{\nabla} \hat{T}_0 \times \hat{\nabla} \hat{n}_1`` term, thus the diamagnetic drift can be approximated as
```math
\mathbf{v}_{i\ast} = c_s \frac{\rho_\mathrm{s}}{a}\frac{1}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times\left(\hat{\nabla}\hat{T}_0 + \hat{\nabla}\hat{T}_1\right).
```
Similar to the ``\mathbf{E}\times\mathbf{B}`` drift, the divergence of the polarization drift gives
```math
\nabla\cdot\mathbf{v}_{i\ast} \approx \frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\frac{1}{\hat{\mathbf{B}} }\hat{\mathbf{b}}  \times \hat{\mathbf{\kappa}} \cdot
\left(\hat{\nabla}\hat{T}_0 + \hat{\nabla} \hat{T}_1\right).
```
The gyroviscous cancellation of the polarization drift gives the following linear terms at lowest order in ``\frac{\rho_\mathrm{s}}{a}``:
```math
\begin{aligned}
\nabla\cdot\left(n_i \mathbf{v}_{pi} + n_i \mathbf{v}_{\pi i}\right) &\approx - \frac{n_i m_i}{e B^2}\nabla\frac{\partial }{\partial t}\left(\nabla_\perp\phi_1 + \frac{1}{n_i e}\nabla_\perp p_i\right)\\
&= -n_{e0}\hat{n}_0\frac{c_s}{a}\frac{c_s^2}{\Omega_i^2}\frac{\partial }{\partial \tau}\left[\nabla\cdot\frac{\hat{\nabla}_\perp}{\rho_\mathrm{s}}\frac{\rho_\mathrm{s}}{a}\hat{\phi}_1 + \nabla\cdot\left(\frac{\hat{\nabla}_\perp}{\rho_\mathrm{s}}\frac{\rho_\mathrm{s}}{a}\hat{T}_1 + \frac{\hat{T}_0}{\hat{n}_0}\frac{\rho_\mathrm{s}}{a}\frac{\hat{\nabla}_\perp}{\rho_\mathrm{s}}\hat{n}_1\right)\right] \\
&= -n_{e0}\hat{n}_0\frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\rho_\mathrm{s}^2\frac{\partial }{\partial \tau}\left[\frac{1}{\rho_\mathrm{s}^2}\hat{\nabla}_\perp^2\hat{\phi}_1 + \frac{1}{\rho_\mathrm{s}^2}\hat{\nabla}_\perp^2\hat{T}_1 + \frac{1}{\hat{n}_0 \rho_\mathrm{s} a}\hat{\nabla}_\perp \hat{T}_0 \cdot \hat{\nabla}_\perp \hat{n}_1 + \frac{1}{\rho_\mathrm{s}^2} \frac{\hat{T}_0}{\hat{n}_0}\hat{\nabla}_\perp^2 \hat{n}_1\right] \\
&= -n_{e0}\hat{n}_0\frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\frac{\partial }{\partial \tau}\left[\hat{\nabla}_\perp^2\hat{\phi}_1 + \hat{\nabla}_\perp^2\hat{T}_1 + \frac{\hat{T}_0}{\hat{n}_0}\hat{\nabla}_\perp^2\hat{n}_1\right]
\end{aligned}
```
In the cold ion approximation, the last term drops.  The last term in the ion continuity equation is given by
```math
\hat{\mathbf{b}} \cdot \nabla\left(n_i v_\parallel\right) = n_{e0}\hat{n}_0 \frac{c_s}{a} \frac{1}{\sqrt{g}\hat{\mathbf{B}} }\frac{\partial }{\partial \theta}\frac{\rho_\mathrm{s}}{a} \hat{v}_\parallel = n_{e0}\hat{n}_0\frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\frac{1}{\sqrt{g}\hat{\mathbf{B}} }\frac{\partial \hat{v}_\parallel}{\partial \theta}.
```
All of these terms may be combined for the normalized ion continuity equation:
```math
\begin{aligned}
&n_{e0}\frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\frac{\partial \hat{n}_1}{\partial \tau} + n_{e0}\hat{n}_0 \frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\frac{1}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times
\hat{\mathbf{\kappa}}\cdot \hat{\nabla} \hat{\phi}_1 +
n_{e0}\hat{n}_0\frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\frac{2}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times\hat{\mathbf{\kappa}}\cdot\left(\hat{\nabla} \hat{T}_0 + \hat{\nabla}
\hat{T}_1\right) - \\
&n_{e0}\hat{n}_0 \frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\frac{\partial }{\partial \tau}\left[\hat{\nabla}_\perp^2\hat{\phi}_1+
\hat{\nabla}_\perp^2\hat{T}_1\right] + n_{e0}\hat{n}_0\frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}\frac{1}{\sqrt{g}\hat{\mathbf{B}} }\frac{\partial \hat{v}_\parallel}{\partial \theta} = 0.
\end{aligned}
```
Dropping the common factor of ``n_{e0}\hat{n}_0\frac{c_s}{a}\frac{\rho_\mathrm{s}}{a}`` and using the quasineutrality condition ``n_i = n_e`` with
the adiabatic electron assumption ``\hat{\phi}_1 = \hat{n}_1/\hat{n}_0``, we have the following ion continuity equation:
```math
\frac{\partial }{\partial \tau}\left[\left(1-\hat{\nabla}_\perp^2\right)\hat{\phi}_1 - \hat{\nabla}_\perp^2\hat{T}_1\right] +
\frac{2}{\hat{\mathbf{B}} }\hat{\mathbf{b}} \times\hat{\mathbf{\kappa}}\cdot\hat{\nabla}\left(\hat{\phi}_1+\hat{T}_0+\hat{T}_1\right) +
\frac{1}{\sqrt{g}\hat{\mathbf{B}} }\frac{\partial \hat{v}_\parallel}{\partial \theta} = 0.
```
