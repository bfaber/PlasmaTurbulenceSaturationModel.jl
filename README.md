# PlasmaTurbulenceSaturationModel

[![Stable](https://img.shields.io/badge/docs-stable-blue.svg)](https://wistell.gitlab.io/PlasmaTurbulenceSaturationModel.jl/stable)
[![Build Status](https://gitlab.com/bfaber/PlasmaTurbulenceSaturationModel.jl/badges/master/pipeline.svg)](https://gitlab.com/wistell/PlasmaTurbulenceSaturationModel.jl/pipelines)
[![Coverage](https://gitlab.com/bfaber/PlasmaTurbulenceSaturationModel.jl/badges/master/coverage.svg)](https://gitlab.com/wistell/PlasmaTurbulenceSaturationModel.jl/commits/master)

The `PlasmaTurbulenceSaturationModel (PTSM)` evaluates the linear eigenspectrum of ion-scale drift waves in toroidal magnetically-confined plasmas.  The eigenspectrum is used to estimate the strength of different nonlinear coupling mechanisms important to plasma microturbulence saturation without directly evaluating a nonlinear model.  The original model implementation replicates the theory described in [Hegna, Terry and Faber, Phys. Plasmas, **25**, 022511 (2018)](https://doi.org/10.1063/1.5018198).

## Algorithm

The `PTSM` algorithm is comprised of three-distinct steps:
  1. Compute the eigenvalues $\omega_i$ from a fluid model for drift-wave instabilities (like Ion Temperature Gradient (ITG) modes and Kinetic Ballooning Modes (KBM)) along a magnetic field line in a toroidal magnetic confinement configuration, such as a stellarator or tokamak.
  2. Compute the quantities describing the coupling between different eigenmodes such as the *three-wave interaction time* 
```math
\tau_{pst}(k,k') = \frac{1}{\mathrm{i}\left(\omega(k'')_t + \omega(k')_s - \omega(k)^\ast_p\right)}
```
and *complex coupling coefficients* 
```math
C(k,k';\omega(k),\omega(k'),\beta(k),\beta(k'))
```
from the linear spectrum.

  3. Construct and compare reduced models or metrics built from the distinct coupling terms to determine important saturation mechanisms.

See the [documentation](https://wistell.gitlab.io/PlasmaTurbulenceSaturationModel.jl/stable) for the detailed steps for using `PTSM.`

### Authors
- Benjamin Faber, University of Wisconsin-Madison
- Joey Duff, University of Wisconsin-Madison
- Aaron Bader, Type One Energy Group
