
"""
    FluidModel{N, T}

Composite type containing the fields required for the saturation calculation.
The `FluidModel` type is parametered by the floating point type `T`,
the number of independent fields `N`. Arrays are always access by either triplet
iteraction or field line index as the first index, the subdominant eigenvalue index
or subdominant interaction index as the second index and the `(kx', ky')` and `(kx, ky)`
indices as the final indices.

# Summary
struct FluidModel <: Any

# Fields
- `plasma::PlasmaParameters` : Plasma parameters
- `grid::SpectralGrid` : Spectral grid parameters
- `problem::LinearProblem` : Linear problem specification
- `geometry::FieldlineGeometry` : Array containing the geometry compoents along the fieldline
- `index_range::Matrix{UnitRange}` : Array holding the range of indices spanned by each mode
- `ω::Array{Complex{T}, 4}` : Array holding the linear eigenfrequencies (`N × n_ev × nkx × nky`)
- `β::Array{Complex{T}, 4}` : Array holding the linear eigenfunctions (`n_points × n_ev × nky`)
- `β²::Array{T, 4}` : Array holding the absolute value squared of the linear eigenfunctions (`n_points × n_ev × nky`)
- `k_perp²::Array{T, 3}` : Array holding the eigenfunction-averaged value of `⟨k⟂²⟩` (`n_ev × nkx × nky`)
- `k_par²::Array{T, 3}` : Array holding the eigenfunction-averaged value of `⟨k∥²⟩`  (`n_ev × nkx × nky`)
- `computed_solution::Matrix{Bool}` : Array indicating current state of solution at each `(kx, ky)` (`nkx × nky`)
- `coupling_index::Array{CartesianIndex, 4}` : Array holding the index of the mode satisfying the three-wave matching condition (`nkx' × nky' × nkx × nky`)
- `coupling_matrix::Array{Complex{T}, 5}` : Array holding the value of the coupling matrix at each point along the field line for each eigenfunction (`N × N × n_ev × nkx × nky`)
- `inv_coupling_matrix::Array{Complex{T}, 5}` : Array holding the value of the inverse coupling matrix at each point along the field line for each eigenfunction (`N × N × n_ev × nkx x nky`) 
- `τ::Array{Complex{T}, 6}` : Array holding the three-wave interaction time for each combination of eigenvalues and wavenumbers (`N × n_ev × nkx' × nky' × nkx × nky`)
- `CC::Array{Complex{T}, 6}` : Array holding the complex coupling coefficients for each combination of eigenvalues and wavenumbers (`N × n_ev × nkx' × nk× nkx × nky`)
- `three_wave_overlap{T, 5}` : Array holding the field line averaged value of `⟨β₁²β₂²β₃²⟩` for modes `β₁`, `β₂`, `β₃` (`n_ev × nkx' × nk× nkx × nky`)
- `overlap_ranges::Array{NTuple{4, UnitRange}, 4}` : Array holding a 4-tuple of the local index ranges for each overlapping mode as well as the global index range of the overlap (`nkx' × nky' × nkx × nky`)
- `amplitudes::Array{NTuple{3, T}, 4}` : Array holding a 3-tuple with relative amplitude weights for the three interacting modes (`nkx' × nky' × nkx × nky`)
- `triplet_combos::Vector{NTuple{3, Int}}` : Vector holding the unique combinations of sub-dominant mode interactions (`n_ev`)

# See also: [`PlasmaParameters`](@ref), [`SpectralGrid`](@ref), [`SolverMethod`](@ref), [`LinearProblem`](@ref)
"""
# The fluid model should hold the results of the linear mode calculations
# that can then be used to calculate the couplings
struct FluidModel{N, T}
    plasma::PlasmaParameters
    grid::SpectralGrid
    problem::LinearProblem
    geometry::FieldlineGeometry
    index_range::Matrix{UnitRange}
    ω::Array{Complex{T}, 4}
    β::Array{Complex{T}, 4}
    β²::Array{T, 4}
    k_perp²::Array{T, 3}
    k_par²::Array{T, 3}
    computed_solution::Matrix{Bool}
    coupling_index::Array{CartesianIndex, 4}
    coupling_matrix::Array{Complex{T}, 5}
    inv_coupling_matrix::Array{Complex{T}, 5}
    τ::Array{Complex{T}, 6}
    CC::Array{Complex{T}, 6}
    three_wave_overlap::Array{T, 5}
    overlap_ranges::Array{NTuple{4, UnitRange}, 4}
    amplitudes::Array{NTuple{3, T}, 4}
    triplet_combos::Vector{NTuple{3, Int}}
end

function FluidModel(plasma::PlasmaParameters,
                    grid::SpectralGrid,
                    prob::LinearProblem,
                    geom::FieldlineGeometry;
                    n_fields::Int = 3,
                    n_ev::Int = 1,
                    FloatType::Type=Float64,
                   )
    return FluidModel(Val(n_fields), plasma, grid, prob, geom; n_ev, FloatType)
end

Base.summary(x::FluidModel{N, T}) where {N, T} = "FluidModel{$N, $T}"
Base.show(io::IO, ::MIME"text/plain", x::FluidModel) = show(io, x)
function Base.show(io::IO, x::FluidModel{N, T}) where {N, T}
    println(summary(x))
end
"""
    n_ev(model::FluidModel)

Retrieve the number of dominant + subdominant modes from the `problem` member of `model`
"""
function n_ev(model::FluidModel)
    return n_ev(model.problem)
end

"""
    n_kx_modes(model::FluidModel)

Retrieve the number of kx modes from the `grid` member of `model`
"""
function n_kx_modes(model::FluidModel;
                    neg_kx::Bool = true,
                   )
  return n_kx_modes(getfield(model,:grid); neg_kx)
end

"""
    n_ky_modes(model::FluidModel)

Retrieve the number of ky modes from the `grid` member of `model`
"""
function n_ky_modes(model::FluidModel)
  return n_ky_modes(getfield(model,:grid))
end

"""
    Δkx(model::FluidModel)

Retrive the kx grid spacing from the `grid` member of `model`
"""
function Δkx(model::FluidModel)
  return Δkx(getfield(model,:grid))
end

"""
    Δky(model::FluidModel)

Retrieve the ky grid spacing from the `grid` member of `model`
"""
function Δky(model::FluidModel)
  return Δky(getfield(model,:grid))
end

"""
    n_fields(model::FluidModel)

Retrieve the number of dynamical fields from `model`
"""
function n_fields(model::FluidModel{N, T}) where {T, N}
    return N
end

"""
    kx(model::FluidModel; negative_modes = false)

Retrieve independent `kx` values for the model, if `negative_modes = true`
the returned array is [0, kxₘᵢₙ, …, kxₘₐₓ, -kxₘₐₓ, …, -kxₘᵢₙ]` 
"""
function kx(model::FluidModel;
            negative_modes=false,
           )
  return kx(getfield(model, :grid); neg_kx = negative_modes)
 #[(model.grid.k_perp.kx[1,1]:Δkx(model):model.grid.k_perp.kx[div(n_kx_modes(model),2)+1,1]);] : [(model.grid.k_perp.kx[div(n_kx_modes(model),2)+2,1]:Δkx(model):model.grid.k_perp.kx[div(n_kx_modes(model),2)+1]);]
end

"""
    kx(model::FluidModel; negative_modes = false)

Retrieve independent `ky` values for the model
"""
function ky(model::FluidModel;)
  return ky(getfield(model, :grid))
end

#=
function eigenvalues(model::FluidModel,
                     kxIndex::Integer,
                     kyIndex::Integer,
                     modeIndex::Integer=0;
                    )
  return modeIndex == 0 ? model.eigenvalues[kxIndex,kyIndex] : model.eigenvalues[kxIndex,kyIndex][modeIndex]
end

function eigenvector(model::FluidModel,
                     kxIndex::Integer,
                     kyIndex::Integer,
                     vectorIndex::Integer=1;
                     array::Bool=false,
                    )
  if !array
    return model.eigenvectors[kxIndex,kyIndex]
  else
    evec = model.eigenvectors[kxIndex,kyIndex]
    return evec.itp.itp.coefs[evec.itp.itp.parentaxes...]
  end
end

function points(model::FluidModel,
                kxIndex::Integer,
                kyIndex::Integer;
               )
  return model.points[kxIndex,kyIndex]
end

function indices(model::FluidModel,
                 kxIndex::Integer,
                 kyIndex::Integer;
                )
  return model.indices[kxIndex,kyIndex]
end
=#
