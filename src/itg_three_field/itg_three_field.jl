include("compute_spectrum.jl")
include("linear_operators.jl")
include("linear_solvers.jl")
include("coupling.jl")

function FluidModel(::Val{3},
                    plasma::PlasmaParameters,
                    grid::SpectralGrid,
                    prob::LinearProblem,
                    geom::FieldlineGeometry;
                    n_fields::Int = 3,
                    n_ev::Int = 1,
                    FloatType::Type=Float64,
                   )
    E = n_ev
    T = FloatType
    n_kx = n_kx_modes(grid)
    n_kx_pos = n_kx_modes(grid; neg_kx = false)
    n_ky = n_ky_modes(grid)

    index_range = map(I -> find_mode_index_range(grid.k_perp[I].kx, grid.k_perp[I].ky, geom, prob), CartesianIndices(grid.k_perp))
    ω = Array{T}(undef, 3, n_ev, n_kx, n_ky)
    β = Array{Complex{T}}(undef, prob.n_points, n_ev, n_kx, n_ky)
    β² = Array{T}(undef, prob.n_points, n_ev, n_kx, n_ky)
    k_perp² = Array{T}(undef, n_ev, n_kx, n_ky)
    k_par² = similar(k_perp²)
    computed_solution = Matrix{Bool}(undef, n_kx, n_ky)
    fill!(computed_solution, false)
    coupling_index = Array{CartesianIndex{2}, 4}(undef, n_kx, n_ky, n_kx_pos, n_ky)
    overlap_ranges = Array{NTuple{4, UnitRange}, 4}(undef, n_kx, n_ky, n_kx_pos, n_ky)
    amplitudes = Array{NTuple{3, T}, 4}(undef, n_kx, n_ky, n_kx_pos, n_ky)
    for I in CartesianIndices((n_kx_pos, n_ky))
        for J in CartesianIndices((n_kx, n_ky))
            K = triplet_matching(I, J, grid)
            coupling_index[J, I] = K
            if K != NoMatchingIndex
                if K[2] > 0
                    overlap_range = intersect(index_range[I], index_range[J], index_range[K])
                    if first(overlap_range) < last(overlap_range)
                        i₀ = first(overlap_range)
                        l₀ = length(overlap_range)
                        overlap_ranges[J, I] = (i₀ - first(index_range[I]) + 1 : i₀ - first(index_range[I]) + l₀,
                                                i₀ - first(index_range[J]) + 1 : i₀ - first(index_range[J]) + l₀,
                                                i₀ - first(index_range[K]) + 1 : i₀ - first(index_range[K]) + l₀,
                                                overlap_range)
                        amplitudes[J, I] = (grid.amplitude[I], grid.amplitude[J], grid.amplitude[K])
                    else
                        overlap_ranges[J, I] = (overlap_range, overlap_range, overlap_range, overlap_range)
                        amplitudes[J, I] = (zero(T), zero(T), zero(T))
                    end
                else
                    overlap_range = intersect(index_range[I], index_range[J])
                    if first(overlap_range) < last(overlap_range)
                        i₀ = first(overlap_range)
                        l₀ = length(overlap_range)
                        overlap_ranges[J, I] = (i₀ - first(index_range[I]) + 1 : i₀ - first(index_range[I]) + l₀,
                                                i₀ - first(index_range[J]) + 1 : i₀ - first(index_range[J]) + l₀,
                                                1:0, overlap_range)
                        amplitudes[J, I] = (grid.amplitude[I], grid.amplitude[J], one(T))
                    else
                        overlap_ranges[J, I] = (overlap_range, overlap_range, overlap_range, overlap_range)
                        amplitudes[J, I] = (zero(T), zero(T), zero(T))
                    end
                end
            else
                overlap_ranges[J, I] = (1:0, 1:0, 1:0, 1:0) 
                amplitudes[J, I] = (zero(T), zero(T), zero(T))
            end
        end
    end
    c_mat = Array{Complex{T}}(undef, 3, 3, n_ev, n_kx, n_ky)
    inv_c_mat = similar(c_mat)
    τ = Array{Complex{T}}(undef, 3, n_ev ^ 3, n_kx, n_ky, n_kx_pos, n_ky)
    CC = similar(τ)
    three_wave_overlap = Array{T}(undef, n_ev ^ 3, n_kx, n_ky, n_kx_pos, n_ky)
    triplet_combos = [(div(i - 1, E ^ 2) + 1, div(rem(i - 1, E ^ 2), E) + 1, rem(rem(i - 1, E ^ 2), E) + 1) for i in 1:E ^ 3]
    return FluidModel{3, T}(plasma, grid, prob, geom,index_range, ω, β, β², k_perp², k_par², computed_solution,
                            coupling_index, c_mat, inv_c_mat, τ, CC, three_wave_overlap, overlap_ranges, amplitudes, triplet_combos)
end
