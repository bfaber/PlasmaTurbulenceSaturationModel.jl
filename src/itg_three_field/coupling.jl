
function coupling_matrix!(model::FluidModel{3, T},
                          I::CartesianIndex,
                          mode_index::Int;
                         ) where {T}
    coupling_matrix!(model.coupling_matrix, model.inv_coupling_matrix,
                     I, mode_index, model.ω, model.plasma, model.grid)
    return nothing
end

function coupling_matrix!(coupling_matrix::AbstractArray,
                          inv_coupling_matrix::AbstractArray,
                          I::CartesianIndex,
                          mode_index::Int,
                          ω::AbstractArray,
                          plasma::PlasmaParameters{T},
                          grid::SpectralGrid{T}
                         ) where {T}
    ω = ω[:, mode_index, I]
    ky = grid.k_perp[I].ky
    ηᵢ = plasma.∇T - 2 / 3 * plasma.∇n
    τ = plasma.τ
    m₁₁ = 1.0
    m₂₁ = ky * ηᵢ / ω[1]
    m₂₂ = ky * ηᵢ / ω[2]
    m₂₃ = ky * ηᵢ / ω[3]
    m₃₁ = im / ω[1] * (1 + 5 / 3 * τ + τ * ky * ηᵢ / ω[1])
    m₃₂ = im / ω[2] * (1 + 5 / 3 * τ + τ * ky * ηᵢ / ω[2])
    m₃₃ = im / ω[3] * (1 + 5 / 3 * τ + τ * ky * ηᵢ / ω[3])
    #mass_mat = [m₁₁ m₁₁ m₁₁;
    #            m₂₁ m₂₂ m₂₃;
    #            m₃₁ m₃₂ m₃₃]
    #mass_mat_inv = inv(mass_mat)
    c_mat_view = view(coupling_matrix, :, :, mode_index, I)
    inv_c_mat_view = view(inv_coupling_matrix, :, :, mode_index, I)
    c_mat_view[:] .= [m₁₁, m₂₁, m₃₁, m₁₁, m₂₂, m₃₂, m₁₁, m₂₃, m₃₃]
    inv_c_mat_view[:, :] .= inv(c_mat_view)
    return nothing
end

function compute_couplings!(model::FluidModel{N, T}) where {N, T}
    compute_couplings!(model.τ, model.CC, model.ω, model.coupling_matrix,
                       model.inv_coupling_matrix, model.coupling_index,
                       model.triplet_combos, model.grid)
    return nothing
end

function compute_couplings!(τ::AbstractArray,
                            CC::AbstractArray,
                            ω::AbstractArray, 
                            coupling_matrix::AbstractArray,
                            inv_coupling_matrix::AbstractArray,
                            coupling_index::AbstractArray,
                            triplet_combos::AbstractArray,
                            grid::SpectralGrid{T},
                           ) where {T}

    indices = CartesianIndices(τ)

    I = [NoMatchingIndex]
    J = [NoMatchingIndex]
    K = [NoMatchingIndex]
    ev_1 = [CartesianIndex(1, 1, 1)]
    ev_2 = [CartesianIndex(1, 1, 1)]
    ev_3 = [CartesianIndex(1, 1, 1, 1)]
    kx₁ = [zero(T)]
    kx₂ = [zero(T)]
    ky₁ = [zero(T)]
    ky₂ = [zero(T)]
    CC₁ = [zero(Complex{T})]
    CC₂ = [zero(Complex{T})]
    ICC₁ = [zero(Complex{T})]
    ICC₂ = [zero(Complex{T})]
    ω₁ = [zero(Complex{T})]
    ω₂ = [zero(Complex{T})]
    ω₃ = [zero(Complex{T})]
    index = [CartesianIndex(1, 1, 1, 1, 1, 1)]
    @inbounds for i in eachindex(τ, CC)
        index[1] = indices[i]
        I[1] = CartesianIndex(index[][5], index[][6])
        ev_1[1] = CartesianIndex(triplet_combos[index[][2]][1], I[])
        J[1] = CartesianIndex(index[][3], index[][4])
        ev_2[1] = CartesianIndex(triplet_combos[index[][2]][2], J[])
        K[1] = coupling_index[J[], I[]]
        ev_3[1] = CartesianIndex(index[][1], triplet_combos[index[][2]][index[][1]], K[])
        if K[1] != NoMatchingIndex
            kx₁[1] = grid.k_perp[I[]].kx
            kx₂[1] = grid.k_perp[J[]].kx
            ky₁[1] = grid.k_perp[I[]].ky
            ω₁[1] = conj(ω[1, ev_1[]])
            ω₂[1] = ω[2, ev_2[]]
            CC₁[1] = coupling_matrix[2, 2, ev_2[]] 
            CC₂[1] = coupling_matrix[3, 2, ev_2[]] 
            ICC₁[1] = inv_coupling_matrix[1, 2, ev_1[]] 
            ICC₂[1] = inv_coupling_matrix[1, 3, ev_1[]] 
            if K[][2] != 0 # non-zonal couplings
                ky₂[1] = grid.k_perp[J[]].ky
                ω₃[1] = ω[ev_3[]]
                CC[i] = (kx₁[] * ky₂[] - ky₁[] * kx₂[]) * (ICC₁[] * CC₁[] + 0.5 * ICC₂[] * CC₂[])
                τ[i] = -im/(ω₃[] + ω₂[] - ω₁[])
            elseif grid.kx_map.forward[I[][1]] + grid.kx_map.forward[J[][1]] != 0 # zonal mode couplings
                CC[i] = ky₁[] * (kx₁[] - kx₂[]) * (ICC₁[] * CC₁[] + ICC₂[] * CC₂[])
                τ[i] = -im/(ω₂[] - ω₁[])

            else #
                CC[i] = zero(Complex{T})
                τ[i] = zero(Complex{T})
            end
        else
            CC[i] = zero(Complex{T})
            τ[i] = zero(Complex{T})
        end
    end
    return nothing
end

