
function quasilinear_flux(model::FluidModel{3, T};
                         ) where {T}

    ql_flux = zero(T)
    E = n_ev(model)
    @inbounds for I in CartesianIndices(size(model.ω)[3:4])
        amp = power_spectrum_coeff(I, model.grid)
        for j in 1:E
            γ = imag(model.ω[1, j, I])
            if γ > zero(T)
                k_perp² = model.k_perp²[j, I]
                ql_flux += amp * γ / k_perp²
            end
        end
    end
    return ql_flux
end

#=
function non_zonal_τC!(τC::AbstractMatrix,
                       model::FluidModel{N, T},
                       overlap::AbstractArray;
                      ) where {N, T}

    for J in CartesianIndices(τC)
        τC_view = @view(model.τC[:, :, :, :, J])
        @inbounds dw_1 = model.spectrum[J]
        for I in CartesianIndices(τC_view)
            @inbounds dw_2 = model.spectrum[I[3], I[4]] 
            K = triplet_matching(CartesianIndex(I[3], I[4]), J, model.grid.kx_map)
            amp_factor = !isnothing(K) ? (K[2] != 0 && (K != CartesianIndex(I[3], I[4]) || K != J)) ? (dw_1[I[2]].parameters.amplitude * 
                         dw_2[I[2]].parameters.amplitude * model.spectrum[K][I[2]].parameters.amplitude) ^ 2 : zero(T) : zero(T)
            overlap_value = !isnothing(K) ? (K[2] != 0 && (K != CartesianIndex(I[3], I[4]) || K != J)) ? overlap[I[2], K[1], K[2], I[3], I[4]] : zero(T) : zero(T)
            @inbounds τC[J] +=  amp_factor * overlap_value * abs(τC_view[I][1] * τC_view[I][2])
        end
    end
    return nothing
end


function non_zonal_τC(model::FluidModel{N, T}, overlap::AbstractArray;) where{N, T} τC = zeros(T, (n_kx_modes(model.grid, neg_kx = false), n_ky_modes(model.grid))) 
    non_zonal_τC!(τC, model, overlap)
    return τC
end

function zonal_τC!(τC::AbstractMatrix,
                   model::FluidModel{N, T},
                   overlap::AbstractArray;
                  ) where {N, T}

    nkx = n_kx_modes(model.grid)
    @inbounds for J in CartesianIndices(τC)
        τC_view = @view(model.τC[1, :, :, J[2], J])
        dw_1 = model.spectrum[J]
        for I in CartesianIndices(τC_view)
            dw_2 = model.spectrum[I[2], J[2]] 
            K = triplet_matching(CartesianIndex(I[2], J[2]), J, model.grid.kx_map)
            amp_factor = !isnothing(K) ? (K[2] == 0 && (K[1] != I[2] || K[1] != nkx - I[2] + 2)) ? (dw_1[I[2]].parameters.amplitude * 
                         dw_2[I[1]].parameters.amplitude ^ 2) ^ 2 : zero(T) : zero(T)
            overlap_value = !isnothing(K) ? (K[2] == 0 && (K[1] != I[2] && K[1] != nkx - I[2] + 2)) ? overlap[I[2], I[2], J[2], J[1], J[2]] : zero(T) : zero(T)
            @inbounds τC[J] +=  amp_factor * overlap_value * abs(τC_view[I][1] * τC_view[I][2])
        end
    end
    return nothing
end

function zonal_τC(model::FluidModel{N, T},
                  overlap::AbstractArray;
                 ) where{N, T}
    τC = zeros(T, (n_kx_modes(model.grid, neg_kx = false), n_ky_modes(model.grid))) 
    zonal_τC!(τC, model, overlap)
    return τC
end


function mean_τ(model::FluidModel{N, T}) where {N, T}
    return sum(abs.(map(τC -> τC[1], model.τC))) / length(model.grid.k_perp)
end

function mean_τC(model::FluidModel{N, T}) where {N, T}
    return sum(abs.(map(τC -> τC[1] * τC[2], model.τC))) / length(model.grid.k_perp)
end

function quasilinear_flux_with_nonzonal_τC(model::FluidModel{3, T};
                                           τC_filter = abs,
                                           keep_howmany = 1,
                                          ) where {T}
    overlap_averages = zeros(T, size(model.overlap_range))
    overlap_average!(overlap_averages, model)

    ql_flux = zero(T)
    E = n_ev(model.problem)
    τC_vector = Vector{T}(undef, 3 * E ^ 2 * n_kx_modes(model) * n_ky_modes(model))
    vector_indices = LinearIndices((3, E ^ 2, n_kx_modes(model), n_ky_modes(model)))
    n_τC = min(E ^ 2, keep_howmany)
    @inbounds for I in CartesianIndices((axes(model.τC, 5), axes(model.τC, 6)))
        for ev_1 in 1:E
            k² = model.spectrum[I][ev_1].k².perp
            γ = imag(model.spectrum[I][ev_1][1])
            amp1 = model.spectrum[I][ev_1].parameters.amplitude
            if γ > zero(T)
                for J in CartesianIndices((axes(model.τC, 3), axes(model.τC, 4)))
                    K = triplet_matching(I, J, model.grid.kx_map)
                    if !isnothing(K) && K[2] != 0 && (K != I || K != J)
                        for ev_index in 1:E ^ 2
                            ev_2, ev_3 = divrem(ev_index - 1, E) .+ (1, 1)
                            MIJ = (ev_1 - 1) * E + ev_2 
                            MIK = (ev_1 - 1) * E + ev_3
                            MJK = (ev_2 - 1) * E + ev_3
                            amp2 = model.spectrum[J][ev_2].parameters.amplitude
                            amp3 = model.spectrum[K][ev_3].parameters.amplitude
                            overlap = overlap_averages[MJK, K, J] * overlap_averages[MIJ, J, I] * overlap_averages[MIK, K, I]
                            for τC_index in 1:3
                                τ, C = model.τC[τC_index, (ev_1 - 1) * E + ev_index, J, I]
                                τC = τC_filter(τ * C)
                                index = vector_indices[τC_index, ev_index, J]
                                τC_vector[index] = amp1 * amp2 * amp3 * overlap * τC
                            end
                        end
                    end
                end
                partialsort!(τC_vector, n_τC, rev = true)
                println(τC_vector[1:n_τC])
                ql_flux += γ / k² / mean(τC_vector[1:n_τC])
            end
        end
    end
    return ql_flux
end

function effective_tauC(model::FluidModel{N, T},
                        how_many = 1,
                        filter_func = abs2;
                        n_threads = Threads.nthreads(),
                       ) where {N, T}
    unstable_indices = CartesianIndices((axes(model.τ, 5), axes(model.τ, 6)))
    coupling_indices = CartesianIndices((axes(model.τ, 1), axes(model.τ, 2), axes(model.τ, 3), axes(model.τ, 4)))
    τ_eff = zeros(T, size(unstable_indices))
    τ_temp = Vector{T}(undef, length(coupling_indices))
    τ_perm = [1:length(τ_temp);]
    τ_temp_indices = LinearIndices(size(model.τ)[1:4])
    τ_indices = LinearIndices(model.τ)
    chunks = Iterators.partition(coupling_indices, length(coupling_indices) ÷ n_threads)
    for I in unstable_indices
        println("Evaluating ", I)
        fill!(τ_temp, zero(T))
        tasks = map(chunks) do chunk
            Threads.@spawn effective_τC_loop!(τ_temp, τ_temp_indices, τ_indices, chunk, I, model.ω, model.β, model.τ, model.CC, model.grid, model.geometry, model.problem, filter_func)
        end
        fetch.(tasks)
        sort_indices = partialsortperm!(τ_perm, τ_temp, how_many; rev = true, initialized = true)
        τ_eff[I] = mean(τ_temp[sort_indices])
        println("Evaluated ", I)
    end

    τ_perm = [1:length(τ_temp);]
    τ_sort = Vector{T}(undef, how_many)
    E = n_ev(model)
    @inbounds for I in unstable_indices
        mode_1 = model.grid.spectrum[I]
        for ev_1 in 1:E
            γ = imag(model.ω[1, ev_1, I])
            amp1 = model.grid.spectrum[I].amplitude
            if γ > zero(T)
                for J in stable_indices
                    mode_2 = model.grid.spectrum[J]
                    K = triplet_matching(I, J, model.grid)
                    if !isnothing(K) && K[2] != 0 && (K != I || K != J)
                        mode_3 = model.grid.spectrum[K]
                        for ev_index in 1 : E ^ 2
                            ev_2, ev_3 = divrem(ev_index - 1, E) .+ (1, 1) 
                            MIJ = (ev_1 - 1) * E + ev_2 
                            MIK = (ev_1 - 1) * E + ev_3
                            MJK = (ev_2 - 1) * E + ev_3
                            amp2 = model.grid.spectrum[J].amplitude
                            amp3 = model.grid.spectrum[K].amplitude
                            overlap = amp1 * amp2 * amp3 * three_wave_overlap(mode_1, ev_1, mode_2, ev_2, mode_3, ev_3, model)
                            for p in 1:N
                                temp_index = τ_temp_indices[p, E ^ 2 * (ev_1 - 1) + ev_index, J]
                                τ_index = τ_indices[p, E ^ 2 * (ev_1 - 1) + ev_index, J, I]
                                τ_temp[temp_index] = overlap * filter_func(model.τ[τ_index] * model.CC[τ_index])
                            end
                        end
                    end
                end
            end
        end
        sort_indices = partialsortperm!(τ_perm, τ_temp, how_many; rev = true, initialized = true)
        τ_eff[I] = mean(τ_temp[sort_indices])
        println("Evaluated ", I)
    end

    return τ_eff             
end


function effective_τC_loop!(τ_temp, τ_temp_indices, τ_indices, Js, I, ω, β, τ, CC, grid, geom, prob, filter_func)
    mode_1 = grid.spectrum[I]
    amp1 = mode_1.amplitude
    E = Int(size(τ, 2) ^ (1 / 3))
    for J in Js
        mode_2 = grid.spectrum[J[3], J[4]]
        amp2 = mode_2.amplitude
        K = triplet_matching(I, CartesianIndex(J[3], J[4]), grid)
        if !isnothing(K) && K[2] != 0 && (K != I || K != CartesianIndex(J[3], J[4]))
            mode_3 = grid.spectrum[K]
            amp3 = mode_3.amplitude
            ev_1, r = divrem(J[2] - 1, E ^ 2) .+ (1, 1)
            ev_2, ev_3 = divrem(r - 1, E) .+ (1, 1) 
            overlap = amp1 * amp2 * amp3 * three_wave_overlap(mode_1, ev_1, mode_2, ev_2, mode_3, ev_3, β, geom, prob)
            temp_index = τ_temp_indices[J]
            τ_temp[temp_index] = overlap * filter_func(τ[J, I] * CC[J, I])
        end
    end
    return nothing
end
=#
