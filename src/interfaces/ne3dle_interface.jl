using .NE3DLE
using .GeneTools

"""
For using NE3DLE, run surface = call_NE3DLE("path_to/NE3DLE.in").
The NE3DLESurface will be the equilibrium information for setupLinearModel().
"""
function setup_linear_model(surface::NE3DLE.NE3DLESurface;
                            n_fields=3,
                            n_ev=1,
                            prec=Float64,
                            Δkx = 0.1,
                            Δky = 0.1,
                            kxSpecLimit = 0.2,
                            kySpecLimit = 0.6,
                            kxSpecPower = 2.0,
                            kySpecPower = 4/3,
                            ∇T = 3.0,
                            ∇n = 0.0,
                            τ = 1.0,
                            β = 1.0,
                            solver=ArnoldiMethod(),
                            soln_interval=8π,
                            ε=400.0,
                            errorOrder=4,
                            hyperdiffusionOrder=2,
                            n_points = 1024,
                            flip_K1_sign::Bool = false
                           )
    
    iota = surface.params.iota;
    nfp = surface.params.nfp;
    s_hat = -(surface.norms.dpsi_drho*surface.norms.ρ)/iota*surface.params.surface_quant["grad_iota"]

    plasma = PlasmaParameters(∇T,∇n,τ,β, FloatType = prec)
    
    problem = LinearProblem(solver=solver, n_ev = n_ev, n_points = n_points,
                            soln_interval=soln_interval, ε=ε,error_order=errorOrder,
                            hyper_diffusion_order=hyperdiffusionOrder, FloatType = prec)

    δkx = convert(prec, Δkx)
    δky = convert(prec, Δky)
    nkx_pos =floor(Int, one(prec) / δkx) + 1

    max_θ = δkx * nkx_pos/(abs(s_hat) * δky) + soln_interval
    n_fieldline_points = soln_interval>0 ? convert(Int, ceil(2 * max_θ * n_points / soln_interval)) : n_points

    # Defined in θ
    θ_range = range(start = -max_θ, stop = max_θ, length = n_fieldline_points)
    
    surface.params.computed_field_periods = (max_θ*iota+surface.params.field_line_label)/(nfp*iota*π)
    surface.params.parallel_resolution = n_fieldline_points
    field_line = NE3DLEFieldLine(surface)    
    p = NE3DLE.PestFromNE3DLE()(field_line)

    metric, modB, sqrtg, K1, K2, ∂B∂θ = gene_geometry_coefficients(GeneFromPest(),p,surface)
    #θ = map(i->-i.ζ*iota,p)
    geom = FieldlineGeometry(metric, modB, sqrtg, (1-2*flip_K1_sign)*K1, K2, ∂B∂θ, θ_range; ι = iota, s_hat = s_hat, nfp = nfp, FloatType = prec)

    grid = PTSM.SpectralGrid(geom, problem; Δkx=Δkx, Δky=Δky,
                             kxSpecLimit=kxSpecLimit, kySpecLimit=kySpecLimit,
                             kxSpecPower=kxSpecPower, kySpecPower=kySpecPower)
    return FluidModel(plasma, grid, problem, geom; n_fields = n_fields, n_ev = n_ev, FloatType=prec)
end
