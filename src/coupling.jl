
"""
    compute_couplings!(model::FluidModel)

Top-level function for computing all the three-wave interaction
times and complex coupling coefficients for interactions
satisfying the three-wave matching condition, storing the results in
`model.τ` and `model.CC`.
"""
function compute_couplings! end

"""
    overlap_integral(x, u, v, sqrtg, modB)
    overlap_integral(x, u, v, w, sqrtg, modB)

Compute the mode overlap integrand at point `x` between 
modes `u` and `v` or `u`, `v`, and `w`.
"""
function overlap_integral(u::AbstractVector,
                          v::AbstractVector,
                          sqrtg::AbstractVector,
                          modB::AbstractVector;
                         )
    @floop SequentialEx() for i in eachindex(u, v, sqrtg, modB)
        dx = sqrtg[i] * modB[i] * conj(u[i]) * v[i]
        @reduce(res = zero(eltype(u)) + dx)
    end
    return res - 0.5 * (sqrtg[1] * modB[1] * conj(u[1]) * v[1] +
                        sqrtg[end] * modB[end] * conj(u[end]) * v[end])
end

function overlap_integral(u::AbstractVector,
                          v::AbstractVector,
                          w::AbstractVector,
                          sqrtg::AbstractVector,
                          modB::AbstractVector;
                         )
    @floop SequentialEx() for i in eachindex(u, v, sqrtg, modB)
        dx = sqrtg[i] * modB[i] * conj(u[i]) * v[i] * w[i]
        @reduce(res = zero(eltype(u)) + dx)
    end
    return res - 0.5 * (sqrtg[1] * modB[1] * conj(u[1]) * v[1] * w[1] +
                        sqrtg[end] * modB[end] * conj(u[end]) * v[end] * w[end])
end

"""
    overlap_integral(mode_1::DriftWave, mode_2::DriftWave, geom::FieldlineGeometry, problem::LinearProblem, field::Union{Int, Symbol})
    overlap_integral(mode_1::DriftWave, mode_2::DriftWave, mode_3::DriftWave, geom::FieldlineGeometry, problem::LinearProblem, field::Union{Int, Symbol})

Compute the overlap between eigenmode field `field` between the modes `mode_1` and `mode_2`
or modes `mode_1`, `mode_2` and `mode_3`.
"""
function overlap_integral(I::CartesianIndex,
                          J::CartesianIndex,
                          K::CartesianIndex,
                          which_ev::NTuple{3, Int},
                          β::AbstractArray,
                          overlap_ranges::AbstractArray,
                          geom::FieldlineGeometry{T};
                        ) where {T}
    if length(last(overlap_ranges[I, J])) > 1
        return overlap_integral(view(β, overlap_ranges[J, I][1], which_ev[1], I),
                                view(β, overlap_ranges[J, I][2], which_ev[2], J),
                                view(β, overlap_ranges[J, I][3], which_ev[3], K),
                                view(geom.jac, overlap_points),
                                view(geom.B, overlap_points))
    else
        return zero(Complex{T})
    end
end

function overlap_integral(I::CartesianIndex,
                          J::CartesianIndex,
                          which_ev::NTuple{3, Int},
                          β::AbstractArray,
                          overlap_ranges::AbstractArray,
                          geom::FieldlineGeometry{T};
                        ) where {T}
    if length(last(overlap_ranges[J, I])) > 1
        return overlap_integral(view(β, overlap_ranges[J, I][1], which_ev[1], I),
                                view(β, overlap_ranges[J, I][2], which_ev[2], J),
                                view(geom.jac, overlap_points),
                                view(geom.B, overlap_points))
    else
        return zero(Complex{T})
    end
end

function three_wave_overlap(I::CartesianIndex,
                            J::CartesianIndex,
                            K::CartesianIndex,
                            which_ev::NTuple{3, Int},
                            β²::AbstractArray,
                            overlap_ranges::NTuple{4, UnitRange},
                            geometry::FieldlineGeometry{T},
                           ) where {T}
    if length(last(overlap_ranges)) > 1
        jac = view(geometry.jac, last(overlap_ranges))
        B = view(geometry.B, last(overlap_ranges))
        β₁ = view(β², overlap_ranges[1], which_ev[1], I) 
        β₂ = view(β², overlap_ranges[2], which_ev[2], J) 
        β₃ = view(β², overlap_ranges[3], which_ev[3], K) 
        return three_wave_integral(jac, B, β₁, β₂, β₃)
    else
        return zero(T)
    end
end

function three_wave_integral(jac::AbstractArray,
                             B::AbstractArray,
                             β₁::AbstractArray,
                             β₂::AbstractArray,
                             β₃::AbstractArray;
                            )
    @floop SequentialEx() for i in eachindex(jac, B, β₁, β₂, β₃)
        dx = jac[i] * B[i] * β₁[i] * β₂[i] * β₃[i]
        @reduce(res = zero(eltype(jac)) + dx)
    end
    return res - 0.5 * (jac[1] * B[1] * β₁[1] * β₂[1] * β₃[1] +
                        jac[end] * B[end] * β₁[end] * β₂[end] * β₃[end])
end

function three_wave_overlap(I::CartesianIndex,
                            J::CartesianIndex,
                            which_ev::NTuple{3, Int},
                            β²::AbstractArray,
                            overlap_ranges::NTuple{4, UnitRange},
                            geometry::FieldlineGeometry{T},
                           ) where {T}
    if length(last(overlap_ranges)) > 1
        jac = view(geometry.jac, last(overlap_ranges))
        B = view(geometry.B, last(overlap_ranges))
        β₁ = view(β², overlap_ranges[1], which_ev[1], I) 
        β₂ = view(β², overlap_ranges[2], which_ev[2], J)
        return three_wave_integral(jac, B, β₁, β₂)
    else
        return zero(T)
    end
end

function three_wave_integral(jac::AbstractArray,
                             B::AbstractArray,
                             β₁::AbstractArray,
                             β₂::AbstractArray;
                            )
    @floop SequentialEx() for i in eachindex(jac, B, β₁, β₂)
        dx = jac[i] * B[i] * β₁[i] * β₂[i]
        dj = jac[i] * B[i]
        @reduce(res = zero(eltype(jac)) + dx)
        @reduce(norm = zero(eltype(jac)) + dj)
    end
    return ((res - 0.5 * (jac[1] * B[1] * β₁[1] * β₂[1] +
                          jac[end] * B[end] * β₁[end] * β₂[end])) /
            (norm - 0.5 * (jac[1] * B[1] + jac[end] * B[end])))
end

"""
    compute_three_wave_overlap!(model::FluidModel)

Top-level function for computing the three-wave overlap value
for all possible interacting modes satisfying the three-wave
matching condition, storing the results in `model.three_wave_overlap`
"""
function compute_three_wave_overlap!(model::FluidModel{N, T}) where {N, T}
    array_inds = CartesianIndices(model.three_wave_overlap)
    @floop ThreadedEx() for i in eachindex(model.three_wave_overlap, array_inds)
        I = CartesianIndex(array_inds[i][4], array_inds[i][5])
        J = CartesianIndex(array_inds[i][2], array_inds[i][3])
        K = model.coupling_index[J, I]
        if K != NoMatchingIndex && K[2] != 0
            model.three_wave_overlap[i] = prod(model.amplitudes[J, I]) * 
                                          three_wave_overlap(I, J, K,
                                                             model.triplet_combos[array_inds[i][1]],
                                                             model.β², model.overlap_ranges[J, I],
                                                             model.geometry)
        else
            model.three_wave_overlap[i] = prod(model.amplitudes[J, I]) *
                                          three_wave_overlap(I, J,
                                                             model.triplet_combos[array_inds[i][1]],
                                                             model.β², model.overlap_ranges[J, I],
                                                             model.geometry)
        end
    end
end

