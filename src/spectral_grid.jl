
"""
    SpectralGrid

Composite type holding the spectral grid properties

# Fields
- `Δk` : `NamedTuple` with labels `(:kx, :ky)`
- `k_perp::Matrix` : Matrix of `NamedTuple`s with labels `(:kx, :ky)`
- `kx_map` : `NamedTuple` used for determining three-wave matching condition
- `η_span::Matrix` : Matrix of `NamedTuple`s with labels `(:min, :max)` with the values of the eigenmode extent along the fieldline in radians
- `amplitude::Matrix` : Matrix holding the relative amplitude of the mode at a particular `(kx, ky)`
- `power_spectrum` : `NamedTuple` with the values used to compute the relative amplitude
"""
struct SpectralGrid{T}
    Δk::NamedTuple
    k_perp::Matrix{NamedTuple}
    kx_map::NamedTuple
    η_span::Matrix{NamedTuple}
    amplitude::Matrix{T}
    power_spectrum::NamedTuple
end

"""
    SpectralGrid(geom::FieldlineGeometry, problem::LinearProblem; kwargs...)

Construct a `SpectralGrid` type from keyword parameters that holds the spectral
discretization parameters for computing the couplings and saturation targets

# Keyword Arguments
- `Δkx::AbstractFloat`=0.05 : Spacing between kx modes in units of 1/ρₛ
- `Δky::AbstractFloat`=0.05 : Spacing between ky modes in units of 1/ρₛ
- `kxSpecLimit::AbstractFloat`=0.2 : kx wavenumber where energy spectrum spectral break occurs
- `kySpecLimit::AbstractFloat`=0.6 : ky wavenumber where energy spectrum spectral break occurs
- `kxSpecPower::AbstractFloat`=2.0 : Exponent of energy spectrum in the kx direction after the spectral break
- `kySpecPower::AbstractFloat`=4/3 : Exponent of energy spectrum in the ky direction after the spectral break
"""
function SpectralGrid(geom::FieldlineGeometry{T},
                      prob::LinearProblem{N, S, T, E};
                      Δkx = 0.1,
                      Δky = 0.1,
                      kxSpecLimit = 0.2,
                      kySpecLimit = 0.6,
                      kxSpecPower = 2.0,
                      kySpecPower = 4/3,
                     ) where{N, S, T, E}
    # Fluid model is invalid for k > 1ρₛ
    δkx, δky = convert.(T, promote(Δkx, Δky))
    nkx = floor(Int, one(T) / δkx) + 1
    nky = floor(Int, one(T) / δky)

    k_perp = Matrix{NamedTuple{(:kx, :ky), Tuple{T, T}}}(undef, 2 * nkx - 1, nky)
    η_span = Matrix{NamedTuple{(:min, :max), Tuple{T, T}}}(undef, 2 * nkx - 1, nky) 
    amplitude = Matrix{T}(undef, 2 * nkx - 1, nky)
    kx_map = circshift(-(nkx-1):(nkx-1), -(nkx-1))
    reverse_map = 1:(2 * nkx - 1)
    for I in CartesianIndices(k_perp)
        ikx = I[1] <= nkx ? I[1] - 1 : -2 * nkx + I[1]
        iky = I[2]
        kx = δkx * ikx
        ky = δky * iky
        amplitude[I] = ((abs(kx) < kxSpecLimit ? 1.0 : (kxSpecLimit/abs(kx))^(kxSpecPower)) *
                        (ky < kySpecLimit ? 1.0 : (kySpecLimit / ky) ^ kySpecPower))
        η₀ = - kx / (geom.parameters.s_hat  * ky)
        η_span[I] = (min = η₀ - 0.5 * prob.soln_interval, max = η₀ + 0.5 * prob.soln_interval)
        k_perp[I] = (kx = kx, ky = ky) 
    end

    return SpectralGrid{T}((kx = δkx,  ky = δky), k_perp,
                           (forward = kx_map, reverse = FFTView(reverse_map)),
                           η_span, amplitude,
                           (kx_limit = kxSpecLimit, kx_exp = kxSpecPower,
                            ky_limit = kySpecLimit, ky_exp = kySpecPower))
end


function Base.size(grid::SpectralGrid{T}) where {T}
  return size(grid.k_perp)
end

function Base.size(grid::SpectralGrid{T},
                   dim::Int;
                  ) where {T}
  return size(grid.k_perp, dim)
end

function Base.length(grid::SpectralGrid{T}) where {T}
    return length(grid.k_perp)
end

function kx(grid::SpectralGrid{T};
            negative_modes = false,
           ) where {T}
    return [grid.k_perp[i, 1].kx for i in 1:n_kx_modes(grid; neg_kx = negative_modes)]
end

function ky(grid::SpectralGrid{T};) where {T}
    return [grid.k_perp[1, i].ky for i in 1:n_ky_modes(grid)]
end

"""
    n_kx_modes(grid::SpectralGrid)

Return the number of kx modes associated with `grid`
"""
function n_kx_modes(grid::SpectralGrid{T};
                    neg_kx::Bool = true,
                   ) where {T}
    return neg_kx ? size(grid, 1) : div(size(grid, 1) + 1, 2)
end

"""
    n_kx_modes(grid::SpectralGrid)

Return the number of ky modes associated with `grid`
"""
function n_ky_modes(grid::SpectralGrid)
    return size(grid, 2)
end

"""
    Δkx(grid::SpectralGrid)

Return the kx grid spacing associated with `grid`
"""
function Δkx(grid::SpectralGrid{T}) where {T}
    return grid.Δk.kx
end

"""
    Δky(grid::SpectralGrid)

Return the ky grid spacing associated with `grid`
"""
function Δky(grid::SpectralGrid{T}) where {T}
    return grid.Δk.ky
end

const NoMatchingIndex = CartesianIndex(-1, -1)

"""
    triplet_matching(K1, K2, grid)
    triplet_matching(K1, K2, kx_map)

Compute the wavenumber `K₃` that satisfies ``K₁ - K₂ - K₃ = 0``, if
the value of `K₃` does not occur in the spectrum, return
`CartesianIndex(-1, -1)`
"""
function triplet_matching(K1::Union{CartesianIndex, NTuple{2, Int}},
                          K2::Union{CartesianIndex, NTuple{2, Int}},
                          grid::SpectralGrid)
    return triplet_matching(K1, K2, getfield(grid, :kx_map))
end

function triplet_matching(K1::Union{CartesianIndex, NTuple{2, Int}},
                          K2::Union{CartesianIndex, NTuple{2, Int}},
                          kx_map::NamedTuple)
    K3_x = kx_map.forward[K1[1]] - kx_map.forward[K2[1]]
    return (minimum(kx_map.forward) <= K3_x <= maximum(kx_map.forward) &&
            kx_map.forward[K1[1]] * K2[2] - kx_map.forward[K2[1]] * K1[2] != 0 ?
            CartesianIndex(kx_map.reverse[K3_x], abs(K1[2] - K2[2])) : NoMatchingIndex)
end

function power_spectrum_coeff(I::CartesianIndex,
                              grid::SpectralGrid;
                             )
    kx = grid.k_perp[I].kx
    ky = grid.k_perp[I].ky
    return ((abs(kx) < grid.power_spectrum.kx_limit ? 1.0 : (grid.power_spectrum.kx_limit/abs(kx))^(grid.power_spectrum.kx_exp)) *
            (ky < grid.power_spectrum.ky_limit ? 1.0 : (grid.power_spectrum.ky_limit / ky) ^ grid.power_spectrum.ky_exp))
end
