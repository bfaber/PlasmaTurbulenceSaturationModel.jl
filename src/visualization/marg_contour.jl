function plotMarginalContours(model::PlasmaTurbulenceSaturationModel.FluidModel;
                           resolution=(900,600),
                           fontsize=25,
                           colormap=:vik,
                           clevels=25,
                  			   title= "Marginal mode frequencies",
                           negativeModes=false,
                          )
  kx = PlasmaTurbulenceSaturationModel.kx(model,negativeModes=true)
  ky = PlasmaTurbulenceSaturationModel.ky(model)
  kx2 = div(length(kx),2)
  startKx = negativeModes ? 1 : kx2+1
  evals = model.eigenvalues
  marginal_ω = circshift(map(x->-real(x[3]),evals), (kx2,0))


  f = Figure(resolution=resolution)

  fontsize_theme = Theme(fontsize = fontsize)
  set_theme!(fontsize_theme)

  Axis(f[1, 1], title = title, xlabel = L"k_x\rho_s", ylabel = L"k_y\rho_s", xlabelsize=fontsize, ylabelsize=fontsize)
  co = contourf!(kx[startKx:end], ky, marginal_ω[startKx:end,:], levels = clevels, colormap=colormap)

  Colorbar(f[1, 2], co, label=L"\gamma (c_s/a)")

  f

end
