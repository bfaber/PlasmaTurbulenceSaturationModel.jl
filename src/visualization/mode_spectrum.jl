
function plotModeSpectrum(model::PlasmaTurbulenceSaturationModel.FluidModel;
		resolution=(1600,900),
		fontsize=25,
		title="Mode spectrum",
		)

	kx = PlasmaTurbulenceSaturationModel.kx(model,negativeModes=true)
 	ky = PlasmaTurbulenceSaturationModel.ky(model)
  kx2 = div(length(kx), 2)
	evals = model.eigenvalues
	unstable_γ = circshift(map(x->imag(x[1]),evals), (kx2,0))
	unstable_ωᵣ = circshift(map(x->-real(x[1]),evals), (kx2,0))

	ωᵣlist = map(x->unstable_ωᵣ[x], 1:length(unstable_ωᵣ))
	γlist = map(x->unstable_γ[x], 1:length(unstable_γ))

	f = Figure()

	fontsize_theme = Theme(fontsize = fontsize)
	set_theme!(fontsize_theme)

	ax = Axis(f[1, 1], title = "ωᵣ vs. γ", xlabel = "γ", ylabel = "ωᵣ")
	scatter!(γlist, ωᵣlist)
	hlines!(ax, [0], color=:black)
	vlines!(ax, [0], color=:black)
	
	f
end

