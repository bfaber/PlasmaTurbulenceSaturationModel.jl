#using DelimitedFiles, CairoMakie, GLMakie
#GLMakie.activate!()
#using CairoMakie
#using LaTeXStrings

function plot_gamma_contours(model::PlasmaTurbulenceSaturationModel.FluidModel;
                             resolution=(900,600),
                             fontsize=25,
                             colormap=:inferno,
                             clevels=25,
                  			 title= "Growth rates",
                             font = "new computer modern latin",
                             negative_modes=false,
                             colorrange = nothing,
                            )

    last_kx = negative_modes ? size(model.grid, 1) : div(size(model.grid, 1), 2) + 1
    kx = model.grid.k_perp.kx[1:last_kx, 1]
    ky = model.grid.k_perp.ky[1, :]
    kx2 = div(length(kx), 2)
    unstable_γ = map(m -> imag(m[1][1]), model.spectrum[1:last_kx, :])
    if negative_modes
        unstable_γ = circshift(unstable_γ, (kx2, 0))
        kx = circshift(kx, kx2)
    end

    f = Figure(resolution=resolution, font = font)

    fontsize_theme = Theme(fontsize = fontsize, font = font)
    set_theme!(fontsize_theme)
    gl = GridLayout(f[1, 1])
    ax = Axis(gl[1, 1], title = title, titlefont = font,
                xlabel = L"k_x\rho_s", ylabel = L"k_y\rho_s",
                xlabelsize=fontsize, ylabelsize=fontsize,
                aspect = AxisAspect(abs(last(kx))/last(ky)))
    cof = contourf!(ax, kx, ky, unstable_γ, levels = clevels, colormap=colormap,
    colorrange = isnothing(colorrange) ? (minimum(unstable_γ) >= 0.0 ? 0.0 : minimum(unstable_γ), maximum(unstable_γ)) : colorrange )

    Colorbar(gl[1, 2], cof, label=L"\gamma\, (c_s/a)")
    tightlimits!(ax)
    colgap!(gl, 5)
    f

end
