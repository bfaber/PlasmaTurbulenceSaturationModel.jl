#Can plot either zonal or nonzonal couplings for max(|Re(τC)|) for each (kx,ky)
#Note, heatmap was chosen, because the levels were messed up for colorscale=log10 with contourf
function plot_tau_C(model::PlasmaTurbulenceSaturationModel.FluidModel;
                    resolution=(700,600),
                    fontsize=25,
                    colormap=:vik,
                    clevels=25,
                    cbarlims=nothing,
                    label=L"|\mathrm{Re}(\tau C)|",
                    zonal_couplings::Bool=true,
                    scale=identity
                    )
    kx = PlasmaTurbulenceSaturationModel.kx(model,negativeModes=false)
    ky = PlasmaTurbulenceSaturationModel.ky(model)

    f = Figure(resolution=resolution, font = "CMU Serif")
    
    #This will use max(|Re(τC)|)
    abs_real_tau_C = Array{Float64}(undef,(length(kx),length(ky)))
    for j in eachindex(ky)
        for i in eachindex(kx)
            tau_C_kx_ky = model.τC[:,:,i,j,:,:]
            tau_times_C = Array{ComplexF64}(undef,size(tau_C_kx_ky))
            for k in eachindex(tau_C_kx_ky)
                tau_times_C[k] = tau_C_kx_ky[k][1]*tau_C_kx_ky[k][2]
            end
            abs_real_tau_C[i,j] = zonal_couplings ? maximum(abs.(real.(tau_times_C[1,:,:,j]))) : maximum(abs.(real.(cat(tau_times_C[3,:,:,1:j-1],tau_times_C[3,:,:,j+1:end],dims=3))))
        end
    end
    title = zonal_couplings ? "Zonal" : "Nonzonal"
    Axis(f[1, 1], xlabel = L"k_x\rho_s", ylabel = L"k_y\rho_s",xlabelsize=fontsize, ylabelsize=fontsize, title=title)
    
    if isnothing(cbarlims)
        co = heatmap!(kx, ky, abs_real_tau_C, levels = clevels, colormap = colormap, colorscale=scale)
        Colorbar(f[1, 2], co, label=label, scale=scale)
    else
        co = heatmap!(kx, ky, abs_real_tau_C, levels = clevels, colormap = colormap, colorscale=scale,colorrange=cbarlims)
        Colorbar(f[1, 2], co, label=label, scale=scale)
    end
    println(minimum(abs_real_tau_C))
    println(maximum(abs_real_tau_C))
   
    f
    
end

function plot_gamma_over_tau_C(model::PlasmaTurbulenceSaturationModel.FluidModel;
                    resolution=(700,600),
                    fontsize=25,
                    colormap=:vik,
                    clevels=25,
                    cbarlims=nothing,
                    label=L"\gamma/|\sum_{k_x,k_y}\mathrm{Re}(\tau C)|",
                    scale=identity,
                    negative_modes=false
                    )
    last_kx = negative_modes ? size(model.grid, 1) : div(size(model.grid, 1), 2) + 1
    kx = model.grid.k_perp.kx[1:last_kx, 1]
    ky = model.grid.k_perp.ky[1, :]
    kx2 = div(length(kx), 2)
    unstable_γ = map(m -> imag(m[1][1]), model.spectrum[1:last_kx, :])
    if negative_modes
        unstable_γ = circshift(unstable_γ, (kx2, 0))
        kx = circshift(kx, kx2)
    end

    f = Figure(resolution=resolution, font = "CMU Serif")
    
    #This will use max(|Re(τC)|)
    abs_real_tau_C = Array{Float64}(undef,(length(kx),length(ky)))
    for j in eachindex(ky)
        for i in eachindex(kx)
            tau_C_kx_ky = model.τC[:,:,i,j,:,:]
            tau_times_C = Array{ComplexF64}(undef,size(tau_C_kx_ky))
            for k in eachindex(tau_C_kx_ky)
                tau_times_C[k] = tau_C_kx_ky[k][1]*tau_C_kx_ky[k][2]
            end
            abs_real_tau_C[i,j] = sum(abs.(real.(tau_times_C[1,1,:,j])))+sum(abs.(real.(cat(tau_times_C[3,:,:,1:j-1],tau_times_C[3,:,:,j+1:end],dims=3))))
        end
    end
    Axis(f[1, 1], xlabel = L"k_x\rho_s", ylabel = L"k_y\rho_s",xlabelsize=fontsize, ylabelsize=fontsize)
    
    if isnothing(cbarlims)
        co = heatmap!(kx, ky, unstable_γ./abs_real_tau_C, levels = clevels, colormap = colormap, colorscale=scale)
        Colorbar(f[1, 2], co, label=label, scale=scale)
    else
        co = heatmap!(kx, ky, unstable_γ./abs_real_tau_C, levels = clevels, colormap = colormap, colorscale=scale,colorrange=cbarlims)
        Colorbar(f[1, 2], co, label=label, scale=scale)
    end
    println(sum(unstable_γ./abs_real_tau_C))
    f
    
end