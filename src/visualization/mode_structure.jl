#=
function ϕ(model::PlasmaTurbulenceSaturationModel.FluidModel;
		nkx=Int,
		nky=Int,
		resolution=(1600,900),
		fontsize=25,
		title="Mode structure = ϕ",
		)
	
	evec = model.eigenvectors
	pts = model.points
	evec = circshift(evec, (10,0))
	ptsc = circshift(pts, (10,0))
	abs_evec = evec[nkx,nky] 

	f = Figure()

	fontsize_theme = Theme(fontsize = fontsize)
	set_theme!(fontsize_theme)

	Axis(f[1,1], title = title, xlabel = "θ")
	lines!(ptsc[nkx,nky], abs_evec)

	f

end
=#


function plot_triplet_overlap(model::PlasmaTurbulenceSaturationModel.FluidModel,
                              k_perp1::Union{Tuple{NTuple{2, Int}, Int}, Tuple{CartesianIndex, Int}, Tuple{SpectralPoint, Int}},
                              k_perp2::Union{Tuple{NTuple{2, Int}, Int}, Tuple{CartesianIndex, Int}, Tuple{SpectralPoint, Int}},
                              k_perp3::Union{Tuple{NTuple{2, Int}, Int}, Tuple{CartesianIndex, Int}, Tuple{SpectralPoint, Int}};
                              kwargs...
                             )
    kx1 = k_perp1[1][1]; ky1 = k_perp1[1][2];
    kx2 = k_perp2[1][1]; ky2 = k_perp2[1][2];
    kx3 = k_perp3[1][1]; ky3 = k_perp3[1][2];
    mode_1 = model.spectrum[kx1, ky1].modes[k_perp1[2]]
    mode_2 = model.spectrum[kx2, ky2].modes[k_perp2[2]]
    mode_3 = model.spectrum[kx3, ky3].modes[k_perp3[2]]
    return plot_triplet_overlap(mode_1, mode_2, mode_3; kwargs...)
end

function plot_triplet_overlap(mode_1::DriftWave{N, T, Fields},
                              mode_2::DriftWave{N, T, Fields},
                              mode_3::DriftWave{N, T, Fields};
                              fields::Union{Symbol, Int, Vector{Symbol}, Vector{Int}} = 1,
                              resolution=(800,500),
                              fontsize=25,
                              title=L"|\phi|",
                              font = "new computer modern roman",
                              linewidth = 4,
                              colorscheme = :Dark2_8,                       
                             ) where {N, T, Fields}

    if !(fields isa Vector)
        fields = fill(fields, 3)
    end
    ϕ_1 = real.(mode_1.β[fields[1]].itp.coefs.parent[2:end-1])
    ϕ_1 ./= maximum(abs.(ϕ_1))
    ϕ_2 = real.(mode_2.β[fields[2]].itp.coefs.parent[2:end-1])
    ϕ_2 ./= maximum(abs.(ϕ_2))
    ϕ_3 = real.(mode_3.β[fields[3]].itp.coefs.parent[2:end-1])
    ϕ_3 ./= maximum(abs.(ϕ_3))
    p1 = range(start = mode_1.η_span.min, stop = mode_1.η_span.max, length = length(ϕ_1))
    p2 = range(start = mode_2.η_span.min, stop = mode_2.η_span.max, length = length(ϕ_2))
    p3 = range(start = mode_3.η_span.min, stop = mode_3.η_span.max, length = length(ϕ_3))
    #label_string_1 = LaTeXString(@sprintf("(k_x=%.3f,k_y=%.3f)", mode_1.k_perp.kx, mode_1.k_perp.ky))
    #label_string_2 = LaTeXString(@sprintf("(k_x=%.3f,k_y=%.3f)", mode_2.k_perp.kx, mode_2.k_perp.ky))
    #label_string_3 = LaTeXString(@sprintf("(k_x=%.3f,k_y=%.3f)", mode_3.k_perp.kx, mode_3.k_perp.ky))

	f = Figure(resolution = resolution, font = font)

	fontsize_theme = Theme(fontsize = fontsize, font = font)
	set_theme!(fontsize_theme)
    x_min = minimum([first(p1), first(p2), first(p3)])
    x_min = (1.0 + (x_min < 0 ? 0.05 : -0.05)) * x_min
    x_max = maximum([last(p1), last(p2), last(p3)])
    x_max = (1.0 + (x_max > 0 ? 0.05 : -0.05)) * x_max
	ax = Axis(f[1,1], title = title, titlesize = fontsize,
              xlabel = "distance along fieldline", xlabelsize = fontsize, ylabelsize = fontsize,
              limits = ((x_min, x_max), (-1.0, 1.0)))

    lines!(ax, [x_min, x_max], [0.0, 0.0],
           color = :black, linewidth = linewidth,)
    lines!(ax, p3, ϕ_3, color = :gray, linewidth = linewidth, 
           label = "Coupling", colorscheme = colorscheme)
    lines!(ax, p2, ϕ_2, color = :blue, linewidth = linewidth,
           label = "Stable", colorscheme = colorscheme)
    lines!(ax, p1, ϕ_1, color = :red, linewidth = linewidth,
           label = "Unstable", colorscheme = colorscheme)
    axislegend(ax; position = :rt)
	f
end
