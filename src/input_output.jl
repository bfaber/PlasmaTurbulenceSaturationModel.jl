
function setup_linear_model end

function save(model::FluidModel{N, T};
              filename::AbstractString="ptsm",
             ) where {N, T}
    has_jld_ext = occursin("jld2",filename)
    filestring = has_jld_ext ? filename : filename*".jld2"
    save_object(filestring, model)
    #=
    jldopen(filestring,"w") do file
        addrequire(file, PlasmaTurbulenceSaturationModel)
        addrequire(file, StaticArrays)
        addrequire(file, StructArrays)
        addrequire(file, Interpolations)
        write(file,"Fluid Model",model)
    end
    =#
end

function load(filename::AbstractString;)
  # Test if filename has "jld" extension
    has_jld_ext = occursin("jld2",filename)
    filestring = has_jld_ext ? filename : filename*".jld2"
    model = nothing
    
    try
        model = load_object(filestring)
        #=
        model = jldopen(filestring, ) do file
        read(file,"Fluid Model")
        end
        =#
    catch
    end
    
    !isnothing(model) ? model : error("$filestring is not a valid JLD2 file!")
end
