
"""
    compute_linear_spectrum!(model::FluidModel; restart = false)

Top-level function for computing the linear eigenspectrum of the `model`;
if `restart` = `true`, the calculation will resume from the last computed
solution specified by `model.computed_solution`.
"""
function compute_linear_spectrum!(model::FluidModel{N, T};
                                  restart::Bool = false,
                                 ) where {N, T}
    kx_range = 1:n_kx_modes(model.grid, neg_kx = false)
    ky_range = 1:n_ky_modes(model.grid)
    initial_kx_ky = initial_guess(model)
    initial_ky_index = initial_kx_ky[2]
    start_ky_index = initial_ky_index
    start_kx_index = initial_kx_ky[1]
    scan_dir = :down
    if restart
        restart_ky_index = div(findfirst(model.computed_solution)[2] - 1, n_kx_modes(model.grid, neg_kx = false)) + 1
        restart_kx_index = findfirst(i -> !model.computed_solution[i, restart_ky_index], 1:n_kx_modes(model.grid, neg_kx = false))
        if isnothing(restart_kx_index)
            if restart_ky_index > inital_ky_index
                restart_ky_index += 1
                scan_dir = :up
            elseif restart_ky_index < inital_ky_index
                restart_ky_index -= 1
            end
            restart_kx_index = 1
        end
        start_ky_index = restart_ky_index
        start_kx_index = restart_kx_index
    end

    init = !restart ? true : false
    
    if scan_dir === :down
        for ky_index in start_ky_index:-1:1
            for kx_index in (ky_index == start_ky_index ? start_kx_index : 1):1:last(kx_range)
                @debug "Computing linear solution at (kx,ky) = ($kx_index, $ky_index)"
                compute_linear_mode!(model, CartesianIndex(kx_index, ky_index), init)
                if init
                    init = false
                end
            end
        end
        scan_dir = :up
    end

    for ky_index in (start_ky_index > initial_ky_index ? start_ky_index : initial_ky_index + 1):1:last(ky_range)
        for kx_index in (ky_index == start_kx_index ? start_kx_index : 1):1:last(kx_range)
            @debug "Computing linear solution at (kx,ky) = ($kx_index, $ky_index)"
            compute_linear_mode!(model, CartesianIndex(kx_index, ky_index), init)
        end
    end
    GC.gc()
end


function compute_linear_mode!(model::FluidModel{N, T},
                              I ::CartesianIndex,
                              init::Bool=false,
                             ) where {N, T}

    get_target!(model, I, init)

    if imag(model.problem.target[]) > zero(T) || init
        # Solve for the eigenmodes
        #if iszero(nnz(model.problem.lhs))
        #    setup_sparse_operators!(model.problem)
        #end
        compute_eigenmodes!(I, model)
        # Sort the modes
        #ω_index = sort_modes(kx,ky,pointRange,ωs,βs,geomView,model.plasma,model.problem)
        #β_vec = βs[:,ω_index]./maximum(abs.(βs[:,ω_index]))

        # Update the spectrum
        update_spectrum!(model, I)
        fill!(model.problem.eig_vals, zero(Complex{T}))
    else
        update_spectrum!(model, I)
    end
    return nothing
end

function compute_linear_mode!(model::FluidModel{N, T},
                              inds::Union{AbstractVector{Int}, NTuple{2, Int}},
                              init::Bool = false;
                             ) where {N, T}
    length(inds) == 2 || throw(DimensionMismatch("Require two wave number indicies, received $(length(inds))"))
    compute_linear_mode!(model, CartesianIndex(inds[1], inds[2]), init)
    return nothing
end


function update_spectrum! end

function get_target!(model::FluidModel{N, T},
                     I::CartesianIndex,
                     init::Bool=false
                    ) where {N, T}
    precond_vec = view(model.problem.precond_vec, :)
    if init
        ω, β = get_initial_target(I, model)
        model.problem.target[:] .= real(ω) + T(2.0) * im * imag(ω)
        precond_vec[:] .= β
    else

        prev_index = I - CartesianIndex(1,0)
        
        if prev_index.I[1] < 1
            higher_ky = I[2] < n_ky_modes(model.grid) ? I + CartesianIndex(0, 1) : I
            lower_ky = I[2] > 1 ? I - CartesianIndex(0, 1) : I
            if model.computed_solution[higher_ky]
                if higher_ky != I
                    prev_index = I + CartesianIndex(0, 1)
                else
                    if model.computed_solution[lower_ky]
                        prev_index = I - CartesianIndex(0, 1)
                    else
                        ω, β = get_initial_target(I, model)
                        model.problem.target[:] .= real(ω) + T(1.5) * im * imag(ω)
                        precond_vec[:] .= β
                        return nothing
                    end
                end
            elseif model.computed_solution[lower_ky]
                if lower_ky != I
                    prev_index = I - CartesianIndex(0, 1)
                else
                    ω, β = get_initial_target(I, model)
                    model.problem.target[:] .= real(ω) + T(1.5) * im * imag(ω)
                    precond_vec[:] .= β
                    return nothing
                end
            end
        end
        if prev_index.I[1] >= 1
            model.problem.target[:] .= real(model.ω[1, 1, prev_index]) + T(1.1) * im * imag(model.ω[1, 1, prev_index])
            construct_precond_vector!(Val(N), precond_vec, view(model.β, :, 1, prev_index), model.grid.k_perp[I],
                                      model.index_range[I], model.ω[:, 1, prev_index], model.plasma, model.geometry, model.problem)
        else
            model.problem.target[:] .= zero(Complex{T})
            precond_vec[:] .= rand(Complex{T}, length(precond_vec))
        end
    end
    return nothing
end

function get_initial_target(I::CartesianIndex,
                            model::FluidModel{N, T};
                           ) where {N, T}
    ι = model.geometry.parameters.ι
    nfp = model.geometry.parameters.nfp
    index_range = model.index_range[I]
    σ = nfp > 1 ? π/abs(ι-nfp) : 0.4π
    ϕ = Vector{T}(undef, model.problem.n_points)
    η_range = model.geometry.z_range[index_range]
    map!(η -> gaussian_mode(η, σ), ϕ, η_range)
    parallel_derivative!(model.problem.cache.∂_vec, ϕ, model.problem)

    copyto!(model.problem.cache.g.xx, model.geometry.g.xx[index_range])
    copyto!(model.problem.cache.g.xy, model.geometry.g.xy[index_range])
    copyto!(model.problem.cache.g.yy, model.geometry.g.yy[index_range])
    copyto!(model.problem.cache.B, model.geometry.B[index_range])
    copyto!(model.problem.cache.jac, model.geometry.jac[index_range])
    damping_drive_term!(model.problem.cache.Bk, model.problem.cache.Dk, index_range, zero(T), model.grid.k_perp[I].ky, model.geometry)
    ω = compute_linear_roots(model.grid.k_perp[I], ϕ, model.problem.cache.∂_vec,
                             model.plasma, model.problem)
    return convert(Complex{T}, ω[1]), rand(Complex{T}, N * model.problem.n_points)
end

function initial_guess(model::FluidModel{N, T},
                      ) where {N, T}
    ky_indices = axes(model.grid.k_perp, 2)
    median_ky_index = convert(Int, ceil(median(ky_indices)))
    ι = model.geometry.parameters.ι
    nfp = model.geometry.parameters.nfp
    σ = nfp > 1 ? π/abs(ι-nfp) : 0.4π
    ϕ = Vector{T}(undef, model.problem.n_points)


    for I in CartesianIndices((1, median_ky_index:n_ky_modes(model.grid)))
        index_range = model.index_range[I]
        η_range = model.geometry.z_range[index_range]
        map!(η -> gaussian_mode(η, σ), ϕ, η_range)
        parallel_derivative!(model.problem.cache.∂_vec, ϕ, model.problem)
    
        copyto!(model.problem.cache.g.xx, model.geometry.g.xx[index_range])
        copyto!(model.problem.cache.g.xy, model.geometry.g.xy[index_range])
        copyto!(model.problem.cache.g.yy, model.geometry.g.yy[index_range])
        copyto!(model.problem.cache.B, model.geometry.B[index_range])
        copyto!(model.problem.cache.jac, model.geometry.jac[index_range])
        damping_drive_term!(model.problem.cache.Bk, model.problem.cache.Dk, index_range, zero(T), model.grid.k_perp[I].ky, model.geometry)
    
        ω = compute_linear_roots(model.grid.k_perp[I], ϕ, model.problem.cache.∂_vec,
                                 model.plasma, model.problem)
        if imag(ω[1]) > 0
            return I
        end
    end

    for I = CartesianIndices((1, median_ky_index-1:-1:1))
        index_range = model.index_range[I]
        η_range = model.geometry.z_range[index_range]
        map!(η -> gaussian_mode(η, σ), ϕ, η_range)
        parallel_derivative!(model.problem.cache.∂_vec, ϕ, model.problem)
    
        copyto!(model.problem.cache.g.xx, model.geometry.g.xx[index_range])
        copyto!(model.problem.cache.g.xy, model.geometry.g.xy[index_range])
        copyto!(model.problem.cache.g.yy, model.geometry.g.yy[index_range])
        copyto!(model.problem.cache.B, model.geometry.B[index_range])
        copyto!(model.problem.cache.jac, model.geometry.jac[index_range])
        damping_drive_term!(model.problem.cache.Bk, model.problem.cache.Dk, index_range, zero(T), model.grid.k_perp[I].ky, model.geometry)
    
        ω = compute_linear_roots(model.grid.k_perp[I], ϕ, model.problem.cache.∂_vec,
                                 model.plasma, model.problem)
        if imag(ω[1]) > 0
            return I
        end
    end

    # If we are here, something has probably gone wrong, but try it anyway
    return (1, median_ky_index)
end
